# Where KITTI data will be saved if you run process_kitti.py
# If you directly download the processed data, change to the path of the data.
DATA_DIR = '/media/magnuswa/F610BD61D526BC3C/exjobb_data/datasets/kitti_data/'

# Where model weights and config will be saved if you run kitti_train.py
# If you directly download the trained weights, change to appropriate path.
WEIGHTS_DIR = '/media/magnuswa/F610BD61D526BC3C/exjobb_data/models/prednet_kitti/'

# Where results (prediction plots and evaluation file) will be saved.
RESULTS_SAVE_DIR = '/media/magnuswa/F610BD61D526BC3C/exjobb_data/output/prednet_kitti/'

DATASET_NAME = 'kitti'