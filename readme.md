# Master Thesis Project Page - Code and Supplementary Material

Here you can find code and supplmentary material for my master thesis project Using GAN for Future Frame Prediction. Videos can be found in the supplementary_material folder.

##Gifs
###L1, WGAN, WGAN + L1 

####L1
![L1 1](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190219_2_l1/prediction_plots/plot_1.gif)
![L1 2](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190219_2_l1/prediction_plots/plot_2.gif)

####WGAN
![WGAN 1](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190215_0_cwgan/prediction_plots/plot_1.gif)
![WGAN 2](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190215_0_cwgan/prediction_plots/plot_2.gif)

####WGAN + L1
![WGAN + L1 1](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190220_0_vanilla/prediction_plots/plot_1.gif)
![WGAN + L1 2](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190220_0_vanilla/prediction_plots/plot_2.gif)

###Effect of conditioning

####Without conditioning
![no conditioning 1](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190221_0_training_ratio_1_unconditional/prediction_plots/plot_1.gif)
![no conditioning 2](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190221_0_training_ratio_1_unconditional/prediction_plots/plot_2.gif)

####With conditioning
![conditioning 1](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190225_0_training_ratio_1_conditional/prediction_plots/plot_1.gif)
![conditioning 2](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190225_0_training_ratio_1_conditional/prediction_plots/plot_2.gif)

###Effect of training ratio

####Training ratio 5
![Training Ratio 5 1](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190220_0_vanilla/prediction_plots/plot_1.gif)
![Training Ratio 5 2](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190220_0_vanilla/prediction_plots/plot_2.gif)

####Training ratio 1
![Training Ratio 1 1](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190221_0_training_ratio_1/prediction_plots/plot_1.gif)
![Training Ratio 1 2](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190221_0_training_ratio_1/prediction_plots/plot_2.gif)

###Longer training

####250 epochs
![250 epochs 1](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190220_0_vanilla/prediction_plots/plot_1.gif)
![250 epochs 2](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190220_0_vanilla/prediction_plots/plot_2.gif)

####400 epochs
![400 epochs 1](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190214_0_cwgan_l1_cont/prediction_plots/plot_1.gif)
![400 epochs 2](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190214_0_cwgan_l1_cont/prediction_plots/plot_2.gif)

###Failure case

![L1 0](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190219_2_l1/prediction_plots/plot_0.gif)
![WGAN 0](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190215_0_cwgan/prediction_plots/plot_0.gif)
![WGAN + L1 0](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190220_0_vanilla/prediction_plots/plot_0.gif)
![no conditioning 0](https://bitbucket.org/MagnusWallgren/exjobb/raw/wgan/supplementary_material/190221_0_training_ratio_1_unconditional/prediction_plots/plot_0.gif)
