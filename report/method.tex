\section{Method}
\subsection{Future Frame Prediction Problem}\label{sec:ffpp}
Future frame prediction is the problem to estimate the next frame, or a sequence of next frames, given past frames. Here the focus is the problem of finding the next frame. In general the next frame \(x_{t+1}\) follows an unknown distribution \(P\) that depends on all previous frames
\begin{equation}
P(x_{t+1} | x_{t}, x_{t-1},..., x_1),
\end{equation}
where \(x_1, x_2, ..., x_t\) is a sequence of observed frames. In this setting the video sequence data are natural video sequences, and as such it is only possible to measure one of the real future frames \(x_{t+1}\). The task is to find a function that generates the prediction \(\hat{x}_{t+1}\) that maximizes the probability given previous frames as
\begin{equation}
\hat{x}_{t+1} = G_{\theta}(x_t, r_t) = \max_{x_{t+1}} P(x_{t+1} | x_{t}, x_{t-1},..., x_1),
\end{equation}
where \(G_{\theta}\) is a generator, here implemented as a neural network with weights \(\theta\) trained to generate predictions given the current frame \(x_t\) and the current network state \(r_t\) as input. The generator will sometimes be written as \(G\) without the subscript of the weights, when the weights are not immediately relevant. The generator \(G\) only sees one frame at a time, but can save a representation of previous frames \(x_1,...,x_{t-1}\) as \(r_t\), which endows the network with the ability to encode motion. The learning problem for the network \(G_{\theta}\) is to vary \(\theta\) to find the \(G_{\theta}\) that minimizes some distance measure between the real data \(x_{t+1}\) and the generated samples \(\hat{x}_{t+1}\). This distance between the predictions and the real next frames, denoted as \(L(\hat{x}_{t+1}, x_{t+1})\), is the loss function one wishes to minimize.

Some different loss functions such as adversarial loss and \(L_1\) distance will be explored here. The loss can also measure the distance between network states calculated from real and predicted frames as \(L(\hat{x}_{t+1}, x_{t+1}, \hat{r}_{t+1}, r_{t+1})\) or more generally the distance between a function of the predicted and real frame \(L(u(\hat{x}_{t+1}), u(x_{t+1}))\) for transfer learning. An example of \(u\) could be a utility function of outcomes of actions done based on evidence \(\hat{x}_{t+1}, x_{t+1}\). The loss function is optimized by means of some optimization algorithm based on gradient descent. The loss function should be chosen so that the distribution converges to \(P\) as the loss function is minimized.

To predict arbitrary future frames \(\tau\) steps into the futue, the predictions \(\hat{x}_{t+1}\) are passed to the network as real frames to iteratively generate one frame at a time. Given a sequence \(x_{1,...,t}\) frames \(x_{t+\tau+1}\) can, generally, be generated as
\begin{align}
	\hat{x}_{t+\tau+1} = G(\hat{x}_{t+\tau}, r_{t+\tau}), \quad \text{where } & \hat{x}_{t+\tau} = G(\hat{x}_{t+\tau-1}, r_{t+\tau-1}) &\text{ if } \tau > 1,\\
	& \hat{x}_{t+1} = G(x_t, r_{t}) &\text{ if } \tau = 1.
\end{align}
In this work the focus is on \(\tau=1\).

\subsection{Artificial Neural Networks}
Artificial Neural Networks are systems that use a model of a neuron in a network to learn to approximate a function from some data. The learning algorithms are referred to as training and is done by means of some gradient descent optimization method, minimizing a loss function with respect to the network node weights. An introduction to neural networks can be found in the deep learning book \cite{goodfellow16}. The reader is assumed to be familiar with neural networks, but most of the report can be understood without that background knowledge. 

\subsection{Generative Model} 
% Write briefly about the idea of the model and have the details in figure text
The predictive coding network of \cite{lotter15, lotter17} is used as generator network. The network has layers arranged in a top-down hierarchal fashion, where higher layers in the hierarchy carry more abstract representations of the frames. \cite{lotter15, lotter17} use the name levels for what is usually called layers in artificial neural networks. Here the name layer will be used. The model can be seen in figure \ref{fig:graph_generator_layer}. In each layer there is a convolutional prediction unit that makes a prediction \(\hat{x}_t^l\) of the frame \(x_t^l\) in layer \(l\) in the hierarchy. The prediction error is passed to a convolutional LSTM unit that updates its states \(r_t^l\), which is passed to the prediction unit. The frame \(x_t^0\) is the input image \(x_t\) and the states in higher layers are calculated in a convolutional layer that takes the error \(e_t^{l-1}\) of the lower layer as input. The update rules are the same as in \cite{lotter17} and are listed in \eqref{eq:pn_update}.
\begin{subequations}\label{eq:pn_update}
\begin{align}
& x_t^l = 
\begin{cases}
x_t, &\text{if } l < 0\\
\textsc{MaxPool} (\textsc{ReLU} (\textsc{Conv}(e_t^{l-1}))), \quad &l > 0
\end{cases}\\
& \hat{x}_t^l = \textsc{ReLU}( \textsc{Conv} (r_t^l))\\
& e_t^l = [\textsc{ReLU}(x_t^l - \hat{x}_t^l), \textsc{ReLU}(\hat{x}_t^l - x_t^l)]\\
& r_t^l = \textsc{ConvLSTM} (e_{t-1}^l, r_{t-1}^l, \textsc{Upsample} (r_t^{l+1}))
\end{align}
\end{subequations}

\begin{figure}
\centering
\includegraphics[width=\textwidth,page=3]{graphs}
\caption{This figure shows the states and model elements in layer \(l\) of the PredNet model. These layers are stacked in a hierarchal structure, where \(x_0^t\) is the input image and \(\hat{x}_0^t\) is the predicted image. At higher layers \(x_l^t\) is a more abstract spatial representation of the image dynamics. \(r_l^t\) are recurrent representations of the state of the model, represented as the recurrent state of the \textsc{ConvLSTM} unit. \(e_l^t\) is the prediction error produced by subtracting the input and the prediction and separating it into two population, one for negative errors and one for positive errors. The exact update equations are listed in \eqref{eq:pn_update}.}
\label{fig:graph_generator_layer}
\end{figure}

\subsection{GAN - Generative Adversarial Network}
% Idea summary - How does a gan work?
To train the network the loss function must first be defined. Training a generative model using mean squared error or mean absolute error as loss leads to blurry looking results \cite{lotter17}. A way to combat this is to use adversial loss in a generative adversarial model, GAN \cite{goodfellow14}. The idea behind GAN is to use two adversely trained neural network models, one that generates fake samples, called generator \(G\), and one that distinguishes fake samples from real samples, called critic \(D\). The goal is to train \(G\) to generate samples close to the real samples and using the ability of \(D\) to distinguish between the fake and real samples in a loss function of the form 
\begin{equation}\label{eq:loss_on_frames}
L_D = L(D(x), D(\hat{x})),
\end{equation}
instead of using per-pixel mean squared or mean absolute error. The critic \(D\) is incorporated into the generator loss function in such a way that minimizing the critic loss makes \(G\) try to generate samples that \(D\) cannot distinguish from real samples. At the same time the critic loss has the adverse objective, to distinguish real samples from fake samples, in the critic loss.\\

\subsubsection{Related work on GAN for future frame prediction}
% The idea - what work is it based on, what are new ideas, why are certain choices made?
While GAN is general in that it can be applied to any type of generative model, their typical application is to capture distributions of image sets with no input or only class label inputs. Future frame prediction is closely related to the class of image-to-image translation problem, in which \cite{isola17} took an approach that used GAN conditioned on input images for a wide variety of these problems. While they make the statement that all image-to-image translation problems can be treated the same way, they did not apply it to future frame prediction. In other previous work GANs have been used for future frame prediction. \cite{liang17} and \cite{jang2018} used GAN loss to predict motion and optical flow. For the work presented in this report GAN will be used directly as loss on the image frames \eqref{eq:loss_on_frames}, rather than constructing a loss on motion. This is similar to \cite{mathieu15} who used GAN conditioned on a sequence of input images on different image scales for future frame prediction. Just like in \cite{isola17} the conditioning will be from input image to output image and the GAN loss will be combined with \(L_1\) loss, but instead of using the normal GAN \cite{goodfellow14}, the improved version of Wasserstein GAN \cite{arjovsky17, gulrajani17} is the GAN used here. This is because it does not have problems with mode collapse and the generator trains well even when the critic gets ahead in training, thus making training more reliable than ordinary GAN.\\

\subsubsection{Adversarial model}
% Description of the algorithm
The adversarial model is described in more detail here, with its main components critic loss, generator loss and the training algorithm. The critic loss is as follows.
\begin{equation}\label{eq:critic_loss}
	L_D(x_{t+1}, \hat{x}_{t+1}) = \E \left(D(x_{t+1})\right) - 
	\E \left(D(\hat{x}_{t+1})\right) + 
	\lambda_{gp} \E \left(||\nabla_{\tilde{x}_{t+1}} D(\tilde{x}_{t+1})||_2 -1\right),
\end{equation}
where \(x_{t+1}\sim\mathbb{P}_R\) are real samples, \(\hat{x}_{t+1}\sim\mathbb{P}_G\) are generated samples and \(\tilde{x}_{t+1} \in \mathbb{P}_{\tilde{x}_{t+1}}\) are uniformly sampled on the straight line between the drawn samples \(x_{t+1}\) and \(\hat{x}_{t+1}\). \(D\) is the critic network and \(\lambda_{gp}\) is a loss weight. This is the unconditioned critic loss. The first two terms are the critic losses on real and fake samples. The last term is a penalty on the gradient. The gradient penalty checks the gradient of the critic with respect to the weights at a point uniformly sampled between the generated sample and the real sample and penalizes as it diverges from 1. This is done because the critic has to be 1-Lipschitz in order to be able to use the Kantorovich-Rubinstein duality to obtain the form of 1-Wasserstein distance in \eqref{eq:critic_loss}, which is satisfied when the gradients are less than 1 everywhere \cite{arjovsky17}. Penalizing any divergence from 1, instead of just penalizing the gradients being less than 1, has the added benefit of preventing the vanishing gradient problem.

By minimizing \eqref{eq:critic_loss} the critic will be trained to minimize \(D(x_{t+1})\), the critic value on real samples, and to maximize \(D(\hat{x}_{t+1})\), the critic value on fake samples. The critic value can be interpreted as scores for the samples based on how real they look. To further help the critic, it is conditioned on the generator input frame \(x_t\) by passing the real/fake sample \(x_{t+1}\) or \(\hat{x}_{t+1}\) with \(x_t\). If the critic would not be conditioned on input frames it could at best distinguish realistically looking frames, while a conditioned critic can potentially also take states of object in the input frame into account. Writing this in the loss function it becomes
\begin{equation}\label{eq:critic_loss_conditioned}
	L_D(x_{t+1}, \hat{x}_{t+1}, x_t, \hat{x}_t) = \E \left(D(x_{t+1}, x_t)\right) - 
	\E \left(D(\hat{x}_{t+1}, x_t)\right) + 
	\lambda_{gp} \E (||\nabla_{\tilde{x}_{t+1}} D(\tilde{x}_{t+1}, x_t)||_2 -1).
\end{equation}
A graph visualization of the critic training model is shown in figure \ref{fig:graph_critic_model}.

\begin{figure}
\begin{minipage}[c]{0.6\textwidth}
	\includegraphics[trim={8cm 0 8cm 0}, clip, width=\textwidth,page=1]{graphs}
\end{minipage}
\begin{minipage}[c]{0.3\textwidth}
	\caption{This figure shows a simplified version of the critic computational graph. The input frame \(x_{t-1}\) is passed through the generator \(G\) to generate a prediction \(\hat{x}_t\). \(\tilde{x}_t\) is uniformly sampled on the line between the prediction \(\hat{x}_t\) and the real frame \(x_t\). \(x_t\), \(\hat{x}_t\) and \(\tilde{x}_t\) are passed to the critic along with \(x_{t-1}\) to get the wasserstein loss and the gradient penalty. The losses are summed as in \eqref{eq:critic_loss_conditioned} to get the total critic loss. The loss is applied to every time slice of the input and optimized with back propagation to get the critic weight updates. To be more precise, \(x_{t-1}\) is not actually passed to the generator, but rather \(G\) generates a prediction based on its internal states and updates its internal states using \(x_t\) after generating a prediction. See listing \ref{alg:training_algorithm}.}
	\label{fig:graph_critic_model}
\end{minipage}
\end{figure}

The generator loss function is the following combination of the critic loss on generated samples and \(L_1\)-norm,
\begin{equation}\label{eq:generator_loss}
	L_G = \E \left(D(\hat{x}_{t+1})\right) + \lambda_{L1} \E \left( ||\hat{x}_{t+1} - x_{t+1}||_1 \right).
\end{equation}
and the corresponding conditioned loss is
\begin{equation}\label{eq:generator_loss_conditioned}
	L_G = \E \left(D(\hat{x}_{t+1}, x_t)\right) + \lambda_{L1} \E \left( ||\hat{x}_{t+1} - x_{t+1}||_1 \right).
\end{equation}
Minimizing this loss trains the generator to have a negative critic value on the generated samples, while maintaining low absolute pixel error. The generator training minimizes critic loss on generated samples while the critic training is trying to minimize the negative critic score on generated samples, that is to maximize the critic score on generated samples. These adverse objectives of the critic and generator are trained alternatingly to make the critic better at distinguishing generated samples from real samples and to make the generator better at generating fake samples that can fool the critic.\\

The \(L_1\) loss term is added as in \cite{isola17}, who found that a combination of conditioned adversarial loss and \(L_1\) loss is best for a variety of image-to-image translation tasks. \cite{lotter17} did not use adversarial loss but they had best results using \(L_1\) loss, producing less blurry results than \(L_2\) loss. A graph visualization of the generator training model is shown in figure \ref{fig:graph_generator_model}.

\begin{figure}
\begin{minipage}[c]{0.6\textwidth}
	\includegraphics[trim={10cm 0 10cm 0}, clip, width=\textwidth,page=2]{graphs}
\end{minipage}
\begin{minipage}[c]{0.35\textwidth}
	\caption{This figure shows the generator computational graph. The generator \(G\) takes the input frame \(x_{t-1}\) and makes a prediction \(\hat{x}_t\). The crtic \(D\) is evaluated on \(\hat{x}_t\) with \(x_{t-1}\) to get the wasserstein loss and the \(L_1\) distance between \(x_t\) and \(\hat{x}_t\) is calculated to get the \(L_1\) loss. \(L_1\) loss and wasserstein loss are summed to get the total generator loss. This computation is applied to every time slice of the input and optimized using back propagation to update the generator weights. To be more precise, \(x_{t-1}\) is not actually passed to the generator, but rather \(G\) generates a prediction based on its internal states and updates its internal states using \(x_t\) after generating a prediction. See listing \ref{alg:training_algorithm}.}
	\label{fig:graph_generator_model}
\end{minipage}
\end{figure}

\subsection{Training}
To train the combined model both losses \eqref{eq:critic_loss} and \eqref{eq:generator_loss} are minimized. This is done by alternating between updating the generator \(G_{\theta}\) weights \(\theta\) and the critic \(D_w\) weights \(w\) using Adam steps \cite{kingma14}. The training is done in minibatches of size \(M\), with \(N_D\) minibatches for every batch. \(N_C\) is called the training ratio and defines how many times the critic weights are updated for each generator update. The complete training algorithm is listed in algorithm \ref{alg:training_algorithm}, with a list of hyperparameters in table \ref{tab:hyperparam}. First a batch of \(M \cdot N_C\) real samples are drawn and split into \(N_D\) minibatches. The critic weights are updated using these minibatches and then another minibatch of the same size is made from a combination of samples in each of the minibatches, which is then used to update the generator. The procedure is similar to that in \cite{gulrajani17} but without the sampling and with the conditional critic.\\

The samples are sequences that the generator and critic process frame by frame. For each time step the generator is first asked to predict the current frame using its internal states, and then it receives the real current frame which it uses to update its internal states. This way the generator can only use the past frame to make predictions. The critic receives the current fake or real frame along with the previous real frame. This way the prediction for the first frame in the sequence is based off of nothing, and this has to be taken into account when updating the weights. This is done by weighting the loss for each time step with time loss weights
\begin{align}
\label{eq:time_loss_weights1}
\lambda_{t_D} &= 
	\begin{cases} 
		0, & \text{ if } t = 0\\
		1, & \text{ if } t > 0
	\end{cases}\\
\label{eq:time_loss_weights2}
\lambda_{t_{L1}} &=
	\begin{cases}
		0, & \text{ if } t = 0,\\
		1/(T-1), & \text{ if } t >0,
	\end{cases}
\end{align}
where \(\lambda_{t_D}\) and \(\lambda_{t_{L1}}\) are time loss weights for critic loss and \(L_1\) loss respectively.
\begin{table}
\centering
\begin{tabular}{|l|c|c|}
\hline
Description & Symbol & Default Value\\\hline
Number of training iterations & \(N\) & 31250 (400 epochs)\\
Training ratio & \(N_D\) & 5\\
Minibatch size & \(M\) & 4\\
Sequence length & \(T\) & 10\\
Gradient penalty weight & \(\lambda_{gp}\) & 10\\
\(L_1\) loss weight & \(\lambda_{L1}\) & 100\\
Learning rate & \(\alpha\) & 0.0001\\
Adams momentum weight & \(\beta_1\) & 0.0\\
Adams momentum weight & \(\beta_2\) & 0.9\\
Time loss weights & \(\lambda_{t_D}\), \(\lambda_{t_{L1}}\) & See equation \ref{eq:time_loss_weights1} and \ref{eq:time_loss_weights2}\\
\hline
\end{tabular}
\caption{Hyperparameters}
\label{tab:hyperparam}
\end{table}

\begin{algorithm}
\caption{Training algorithm for GAN model. The required values are hyperparameters and are listed in table \ref{tab:hyperparam}.}
\begin{algorithmic}[0]
\Require number of training iterations \(N\), training ratio \(N_D\), minibatch size \(M\), sequence length \(T\), gradient penalty weight \(\lambda_{gp}\), \(L_1\) loss weight \(\lambda_{L1}\), learning rate \(\alpha\), Adams momentum weights \(\beta_1\) and \(\beta_2\)
\Require time loss weights \(\lambda_{t_D}, \lambda_{t_{L1}}\)
\Require initial generator weights \(\theta\) and critic weights \(w\)
\For{\(n = 1:N\)} % number of generator updates
	\State Draw batch \(\{x_t^{n_D,m}\}^{n_D = 1,...,N_D; m=1,...,M}_{t=0,...,T}\), where \(x_0^{n_D,m}\) is an empty frame
	\State Draw a random number \(\epsilon \sim U[0,1]\).	
	\For{\(n_D = 1:N_D\)} % critic updates
		\For{\(m = 1:M\)} % batch sample index
			\State \(r \leftarrow 0\)
			\For{\(t=1:T\)}
				\State \(x, x_{\text{prev}}  \leftarrow x_t^{n_D,m}, x_{t-1}^{n_D,m}\)
				\State \(\hat{x}, r \leftarrow G_\theta(x, r)\)
				\State \(\tilde{x} \leftarrow \epsilon \hat{x} + (1-\epsilon) x\)
				\State \(L^{(m,t)}_D \leftarrow D_w(\hat{x}, x_{\text{prev}}) - D_w(x, x_{\text{prev}}) 
					+ \lambda_{gp} (||\nabla_{\tilde{x}} D(\tilde{x}, x_{\text{prev}})||_2 -1)\)
			\EndFor			
		\EndFor
		\State \(w \leftarrow \textsc{Adam}(\nabla_w \frac{1}{M T} \sum_{m=1}^M \sum_{t=1}^T \lambda_{t_D} L^{(m,t)}_D, w, \alpha, \beta_1, \beta_2)\)
	\EndFor
	\State Set batch \(\{x_t^m\}_{t=0,...,T}^{m=1,...,M} = \text{Repartion}(\{x_t^{n_D,m}\}^{n_D = 1,...,N_D; m=1,...,M}_{t=0,...,T})\)
	\For{\(m = 1:M\)} % batch
		\State \(r \leftarrow 0\)
		\For{\(t=1:T\)}
			\State \(x, x_{\text{prev}} \leftarrow x_t^{m}, x_{t-1}^{m}\)
			\State \(\hat{x}, r \leftarrow G_\theta(x, r)\)
			\State \(L_G^{(m,t)} \leftarrow - \lambda_{t_D} D_w(\hat{x}) + \lambda_{t_{L1}} \lambda_{L1} ||\hat{x} - x||_1\)
		\EndFor
	\EndFor
	\State \(\theta \leftarrow \textsc{Adam}( \nabla_\theta \frac{1}{M T} \sum_{m=1}^M \sum_{t=1}^TL_G^{(m,t)}, \alpha, \beta_1, \beta_2)\)
\EndFor
\end{algorithmic}
\label{alg:training_algorithm}
\end{algorithm}

\subsection{Critic model}
A visualization of the critic model is shown in figure \ref{fig:graph_critic_layer}. The critic uses a structure with stacked convolutional layers followed by densely connected layers. The benefit of using convolutions is that they use weight sharing, requiring less trainable parameters while still being able to capture information over an area in the image. Because of the weight sharing the convolutional layers are also translationally invariant, which is to some extent an expected property in a next frame prediction model, while the densely connected layers are not. The total receptive field of the convolutional layers can be calculated by tracing the filter sizes and strides of the convolution filters backward. For the parameters used in the experiments, shown in table \ref{tab:critic_param}, the total receptive field is 70x70 pixels.

\begin{figure}
\begin{minipage}[c]{0.6\textwidth}
	\includegraphics[page=4, trim={10cm 0 10cm 0}, clip, width=\textwidth]{graphs}
\end{minipage}
\begin{minipage}[c]{0.35\textwidth}
\caption{This is a visualization of the layers in the critic model. Predictions \(\hat{x}_t\)or real samples are \(x_t\) and are concatenated with the previous frame \(x_{t-1}\) and then passed through convolutional layers with Leaky ReLU as activation function. This is followed by flattening the samples to the shape \([n_{\text{batch}}, T, n_{\text{pixels}} \cdot n_{\text{channels}}]\), a densely connected layer with Leaky ReLU activation and another densely connected layer. Details about filter parameters can be seen in table \ref{tab:critic_param}.}
\label{fig:graph_critic_layer}
\end{minipage}
\end{figure}%\end{wrapfigure}

\begin{table}[h]
\centering
	\begin{tabular}{|lll|}
	\hline
	layer type & filter size & stride\\
	\textsc{Conv2D} & 3x3x16 & 1\\
	\textsc{Conv2D} & 3x3x32 & 1\\
	\textsc{Conv2D} & 3x3x32 & 2\\
	\textsc{Flatten} & & \\
	\textsc{Dense} & 128 &\\
	\textsc{Dense} & 1 &\\
	\hline
	\end{tabular}
	\caption{This table shows the filter sizes and stride of the convolutional layers in the critic and the number of ouput nodes in the densely connected layers. For densely connected layers the filter size is the number of nodes in the layer. A filter size of \(A\)x\(B\)x\(C\) represents a stack of \(C\) filters each covering an area of \(A\)x\(B\) pixels. The number of input channels to each layer is the stack size \(C\) of the previous layer.}
	\label{tab:critic_param}
\end{table}

\subsubsection{Training Ratio}
Training ratio \(N_D\) is a hyperparameter in the WGAN model that defines how many critic weight updates should be done for each generator weight update. \cite{gulrajani17} use sampling, generating new samples for each critic update. However sampling is not well suited for future frame prediction because the problem has a one-to-one mapping between input and output in the data. Potentially there could be variations in possible future frames, due to noise and the random nature of the observed dynamics, but it is still only possible to observe one of the possible future frames. Sampling could  possibly model the differences in scene dynamics, but with only one possible output frame to calculate the loss on, it could easily lead to the model just ignoring the latent input \cite{isola17}.\\

To still be able to vary the number of times the critic is updated per generator update, the batches are divided into minibatches, with the number of minibatches being \(N_D\). Then each critic update uses one of the minibatches and the generator update uses a mixed partition of the minibatches of the same size as a minibatch, as described in algorithm \ref{alg:training_algorithm}. The relation between the number of times the critic is updated per generator update is not what matters here, but rather the amount of data used to update the critic and generator. The critic could almost equivalently be updated with one batch and \(N_D\) times larger learning rate because sampling is not used. Either way the reason for having the training ratio is to give the critic an advantage in training.\\

There is one drawback with this method and that is that the generator can only be trained on \(1/N_D\) part of the training dataset, which can be a big problem. The effect of using the learning rate suggested in \cite{gulrajani17} \(N_D = 5\) compared to using \(N_D = 1\) is done here. It would be interesting to try other learning rates but that will be left to future work, and instead the effect of using scheduled optimization (that is having many critic updates for each generator update) as opposed to alternating optimization.
