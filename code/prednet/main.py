# Misc
import os
import numpy as np
from six.moves import cPickle
import hickle as hkl
import datetime

# Plotting
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.animation as animation

# Keras
import logging
logging.getLogger("tensorflow").setLevel(logging.ERROR)
from keras import backend as K
from keras.models import Model, model_from_json, load_model
from keras.layers import Input, Dense, Flatten

# Local
from prednet_gan import PrednetGan
from prednet import PredNet
from data_utils import SequenceGenerator
from kitti_settings import *
# from pedestrians_settings import *



def write_plots(x,y,n_channels=3,nb_sequences=40,data_format='channels_last',output_mode=None,n_plot=40):
    '''
    Arguments:
        x,y             input output pairs of image sequences
        n_channels  number of channels to show in each plot
        nb_sequences    number of sequences to plot
        data_format     channels_first or channels_last
        output_mode     output_mode used to label plot
    '''
    plot_save_dir = os.path.join(RESULTS_SAVE_DIR, 'prediction_plots/')

    stack_size = y.shape[0]
    nt = y.shape[1]
    if data_format == 'channels_first':
        n_channels_total = y.shape[2]
        nb_row = y.shape[3]
        nb_col = y.shape[4]
        y = K.reshape(y, (stack_size, nt, nb_row, nb_col, n_channels_total))
        x = K.reshape(x, (stack_size, nt, nb_row, nb_col, x.shape[2]))
    else:
        n_channels_total = y.shape[4]
        nb_row = y.shape[2]
        nb_col = y.shape[3]

    # plot in color
    if (output_mode == 'prediction' or output_mode == 'error') and x.shape[-1] > 1:
        n_channels = 1
        plot_color = True
    else:
        plot_color = False

    aspect_ratio = float(nb_row) / nb_col
    
    plot_idx = np.random.permutation(stack_size)[:n_plot]
    ch_idx = np.random.permutation(n_channels_total)


    plt.figure(figsize = (nt, (1+n_channels)*aspect_ratio))
    gs = gridspec.GridSpec((1+n_channels), nt)
    gs.update(wspace=0., hspace=0.)
    plot_save_dir = os.path.join(RESULTS_SAVE_DIR, 'prediction_plots/')
    if not os.path.exists(plot_save_dir): os.mkdir(plot_save_dir)
    
    # x = np.squeeze(x,-1)
    # y = np.squeeze(y,-1)

    for i in plot_idx:
        for t in range(nt):
            plt.subplot(gs[t])
            if x.ndim > 4:
                plt.imshow(x[i,t], interpolation='none')
            else:
                plt.imshow(x[i,t], interpolation='none', cmap='gray')

            plt.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)
            if t==0: plt.ylabel('Actual', fontsize=10)

            if plot_color:
                plt.subplot(gs[t + nt])
                plt.imshow(y[i,t], interpolation='none')
                plt.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)
                if t==0: plt.ylabel(output_mode, fontsize=8)

            else:
                for ch in range(n_channels):
                    plt.subplot(gs[t + nt*(1+ch)])
                    plt.imshow(y[i,t,:,:,ch_idx[ch]], interpolation='none',cmap='gray')
                    plt.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)
                    if t==0: plt.ylabel(output_mode+'_'+str(ch_idx[ch]), fontsize=8)

        plt.savefig(plot_save_dir +  'plot_' + str(i) + '.png')
        plt.clf()

def write_animated_plots(x,y,n_plot):
    
    plot_save_dir = os.path.join(RESULTS_SAVE_DIR, 'prediction_plots/')
    writer = animation.FFMpegWriter()
    
    nrows = x.shape[2]
    ncols = x.shape[3]
    nchannels = x.shape[4]

    x = np.squeeze(x)
    y = np.squeeze(y)
    
    if nchannels==1:
        shp = (nrows,2*ncols)
    else:
        shp = (nrows,2*ncols,nchannels)
    
    fig1 = plt.figure()
    for i in range(min(x.shape[0],n_plot)):
        ims = []
    
        for t in range(x.shape[1]):
            xt = x[i,t]
            yt = y[i,t]
            zt = np.zeros(shp)
            zt[:,range(ncols)] = xt
            zt[:,range(ncols,2*ncols)] = yt
            if np.min(zt) < 0:
                zt = (zt+1.0)/2.0
            ims.append((plt.imshow(zt,cmap='gray'),))
        
        save_file = os.path.join(plot_save_dir,'plot_{}.mp4'.format(i))
        im_ani = animation.ArtistAnimation(fig1, ims, interval=50, repeat_delay=3000, blit=True)
        im_ani.save(save_file, writer=writer)
        plt.clf()

def make_out_dir():
    n = 0
    out_dir = os.path.join(RESULTS_SAVE_DIR, datetime.date.today().strftime("%y%m%d") + '_{}'.format(n))
    while os.path.exists(out_dir):
        n += 1
        out_dir = os.path.join(RESULTS_SAVE_DIR, datetime.date.today().strftime("%y%m%d") + '_{}'.format(n))
    os.mkdir(out_dir)   
    return out_dir
    
def plot_loss(history, out_dir):

    if 'val_loss' in history.history.keys():
        f = os.path.join(out_dir, 'loss' + '.png')
        loss = history.history['val_loss']
        plt.plot(loss,'-',markersize=10)
        plt.xlabel('epoch', fontsize=10)
        plt.ylabel('validation loss', fontsize=10)
        plt.savefig(f)
        plt.clf()
    
    if 'critic_loss' in history.history.keys():
        f1 = os.path.join(out_dir, 'critic_loss' + '.png')
        loss = history.history['critic_loss']
        t = loss['epoch']
        total = loss['total']
        real = loss['real']
        fake = loss['fake']
        gp = loss['gp']
        
        plt.plot(t, total, '-', label='total')
        plt.plot(t, gp,'-', label='gp')
        plt.plot(t, real,'-', label='real')
        plt.plot(t, fake,'-', label='fake')
        plt.xlabel('epoch')
        plt.ylabel('loss')
        plt.title('Critic Validation Loss')
        plt.legend()
        plt.savefig(f1)
        plt.clf()

    if 'gen_loss' in history.history.keys():
        f2 = os.path.join(out_dir, 'generator_loss' + '.png')
        loss = history.history['gen_loss']
        t = loss['epoch']
        total = loss['total']
        w = loss['wass']
        mae = [100.0*x for x in loss['mae']]

        plt.plot(t, total, '-', label='total')
        plt.plot(t, w, '-', label='critic')
        plt.plot(t, mae, '-', label='mae')
        plt.xlabel('epoch')
        plt.ylabel('loss')
        plt.title('Generator Validation Loss')
        plt.legend()
        plt.savefig(f2)
        plt.clf()

    if 'batch_loss' in history.history.keys():
        f3 = os.path.join(out_dir, 'training_loss' + '.png')
        loss = history.history['batch_loss']
        total = loss['total']
        real = loss['real']
        fake = loss['fake']
        gp = loss['gp']
        
        plt.plot(total, '-', label='total')
        plt.plot(gp,'-', label='gp')
        plt.plot(real,'-', label='real')
        plt.plot(fake,'-', label='fake')
        plt.xlabel('training iteration')
        plt.ylabel('loss')
        plt.title('Critic Training Loss')
        plt.legend()
        plt.savefig(f3)
        plt.clf()


def load_prednet_model(output_mode,nt):

    weights_file = os.path.join(WEIGHTS_DIR, 'prednet_weights.hdf5')
    json_file = os.path.join(WEIGHTS_DIR, 'prednet_model.json')
    model_file = os.path.join(WEIGHTS_DIR, 'my_model.h5')

    # Load trained model
    f = open(json_file, 'r')
    json_string = f.read()
    f.close()
    
    train_model = model_from_json(json_string, custom_objects = {'PredNet': PredNet})
    train_model.summary()
    train_model.load_weights(weights_file)
    layer_config = train_model.layers[1].get_config()
    
    layer_config['output_mode'] = output_mode
    # for k in layer_config.keys(): print(k,layer_config[k])
    data_format = layer_config['data_format']
    test_prednet = PredNet(weights=train_model.layers[1].get_weights(), **layer_config)
    input_shape = list(train_model.layers[0].batch_input_shape[1:])
    input_shape[0] = nt
    
    return test_prednet, input_shape, data_format

def compare_mse(x,y):
    assert x.shape == y.shape, 'x.shape must be equal to y.shape.'
    mse_model = np.mean( (x[:, 1:] - y[:, 1:])**2 )  # look at all timesteps except the first
    mse_prev = np.mean( (x[:, 1:] - x[:, :-1])**2 )
    mse_empty = np.mean( ( x[:, 1:] - np.zeros(x[:, 1:].shape) )**2 )
    
    mse_info = "Model MSE: %f\n" % mse_model
    mse_info = mse_info + "Previous Frame MSE: %f\n" % mse_prev
    mse_info = mse_info + "Empty Frame MSE: %f" % mse_empty

    return mse_info

def generate_log(out_dir, dataset_info=None, model_info=None, training_info=None, mse_info=None, history=None, summary=None):

    f = open(os.path.join(out_dir, 'output.log'), 'w')

    if dataset_info is not None:
        f.write("# dataset info\n")
        for key in dataset_info:
            f.write('{:<30}{:<30}\n'.format(key, dataset_info[key]))
        f.write("\n\n")

    if model_info is not None:
        f.write("# model info\n")
        for key in model_info:
            f.write('{:<30}{:<30}\n'.format(key, model_info[key]))
        f.write("\n\n")
    if summary is not None:
        f.write(summary)
        f.write("\n\n")

    if training_info is not None:
        f.write("# training info\n")
        for key in training_info:
            f.write('{:<30}{:<30}\n'.format(key, training_info[key]))
        f.write("\n\n")

    if mse_info is not None:
        f.write("# testing info\n" + mse_info + '\n\n')

    if history is not None:

        f.write("# training history\n")

        if 'val_loss' in history.history.keys():
            for loss in history.history['val_loss']:
                f.write('{:<30}{:<30}\n'.format('validation loss', loss))

        if 'critic_loss' in history.history.keys():
            loss = history.history['critic_loss']
            for k in ['epoch', 'total', 'real', 'fake', 'gp']:
                f.write('{:<20}'.format(k))
            f.write('\n')
            for epoch, total, real, fake, gp in zip(loss['epoch'], loss['total'], loss['real'], loss['fake'], loss['gp']):
                f.write('{:<20}{:<20}{:<20}{:<20}{:<20}\n'.format(epoch, total, real, fake, gp))
            f.write('\n\n')
        if 'gen_loss' in history.history.keys():
            f.write('generator_loss\n')
            loss = history.history['gen_loss']
            for k in ['epoch', 'total', 'wass', 'mae']:
                f.write('{:<20}'.format(k))
            f.write('\n')
            for epoch, total, w, mae in zip(loss['epoch'], loss['total'], loss['wass'], loss['mae']):
                f.write('{:<20}{:<20}{:<20}{:<20}\n'.format(epoch, total, w, mae))
        f.write("\n\n")

    f.close()


if __name__ == '__main__':

    ############################# SETUP #############################
    dataset = DATASET_NAME
    out_dir = make_out_dir()

    # Training parameters
    nb_epoch = 250
    batch_size = 4
    samples_per_epoch = 500
    N_seq_val = 100
    loss = 'mean_absolute_error'
    optimizer = 'adam'

    # mode = 'testing'
    mode = 'training'

    # Data parameters
    n_channels = 3
    im_height = 128
    im_width = 160
    input_shape = (n_channels, im_height, im_width) if K.image_data_format() == 'channels_first' else (im_height, im_width, n_channels)
    nt = 10
    
    # Model parameters
    layer_loss_weights = np.array([1., 0., 0., 0.])  # weighting for each layer in final loss; "L_0" model:  [1, 0, 0, 0], "L_all": [1, 0.1, 0.1, 0.1]
    layer_loss_weights = np.expand_dims(layer_loss_weights, 1)
    time_loss_weights = 1./ (nt - 1) * np.ones((nt,1))  # equally weight all timesteps except the first
    time_loss_weights[0] = 0
    stack_sizes = (n_channels, 48, 96, 192)
    #stack_sizes = (n_channels, 12, 24, 48)
    A_filt_sizes = (3, 3, 3)
    Ahat_filt_sizes = (3, 3, 3 ,3)
    R_filt_sizes = (3, 3, 3, 3)

    ############################# TRAINING #############################
    history=None
    gan = PrednetGan(input_shape, stack_sizes, layer_loss_weights, time_loss_weights, 
        A_filt_sizes=A_filt_sizes, Ahat_filt_sizes=Ahat_filt_sizes, training_ratio=1, R_filt_sizes=R_filt_sizes, nt=nt, tb_dir=out_dir)
    history = gan.train_gan(nb_epoch, batch_size, samples_per_epoch, N_seq_val, resume_training=False)

    ############################# TESTING #############################
    n_plot = 40
    output_mode = 'prediction'

    test_file = os.path.join(DATA_DIR, 'X_test.hkl')
    test_sources = os.path.join(DATA_DIR, 'sources_test.hkl')

    # load model
    test_prednet, input_shape, data_format = load_prednet_model(output_mode=output_mode, nt=nt)
    
    output_shape = test_prednet.compute_output_shape(input_shape)
    inputs = Input(shape=tuple(input_shape))
    predictions = test_prednet(inputs)
    test_model = Model(inputs=inputs, outputs=predictions)
    
    # Create testing model (to output predictions)
    test_generator = SequenceGenerator(test_file, test_sources, nt, sequence_start_mode='unique', data_format=data_format, output_mode='prediction')
    X_test = test_generator.create_all()
    Y_test = test_model.predict(X_test)
    
    ############################# LOGGING #############################

    # Compare MSE of PredNet predictions vs. using last frame.  Write results to prediction_scores.txt
    if output_mode == 'prediction':
        mse_info = compare_mse(X_test,Y_test)
    else:
        mse_info = None

    # Plot some predictions
    print('Writing plots')
    write_animated_plots(x=X_test,y=Y_test, n_plot=n_plot)
    # write_plots(X_test, Y_test, n_channels=3, nb_sequences=40, data_format='channels_last', output_mode='prediction', n_plot=40)
    if history is not None:
        plot_loss(history, out_dir)

    # Generate log
    if mode == 'training':
        # training, model, data config
        training_config =   {'nb_epoch': nb_epoch,
                            'batch_size': batch_size,
                            'samples_per_epoch': samples_per_epoch,
                            'N_seq_val': N_seq_val,
                            'loss': loss,
                            'optimizer': optimizer,
                            'nb_parameters': test_model.count_params()
                            }
    else:
        training_config = None

    data_config = { 'data_dir': DATA_DIR,
                    'im_height': im_height,
                    'im_width': im_width,
                    'n_channels': n_channels
                    }

    model_config = {'input_shape': input_shape,
                    'stack_sizes': stack_sizes,
                    'A_filt_sizes': A_filt_sizes,
                    'Ahat_filt_sizes': Ahat_filt_sizes,
                    'R_filt_sizes': R_filt_sizes,
                    'layer_loss_weights': layer_loss_weights,
                    'nt': nt,
                    'time_loss_weights': time_loss_weights}
    
    print('Generating log')
    generate_log(out_dir, dataset_info=data_config, training_info=training_config, model_info=model_config, mse_info=mse_info, history=history)