'''
Code for processing Caltech Pedestrians data
'''

import os
import numpy as np
from scipy.misc import imread, imresize
import hickle as hkl
from pedestrians_settings import *

desired_im_sz = (128, 160)

def resample(files, sources, sr):
    files = sorted(files)
    files = files[0::sr] + files[1::sr] + files[2::sr]
    source = sources[0]

    current_idx = 0
    for start_idx in range(sr):
        num_files_range = len(range(start_idx,len(files),sr))
        sources[current_idx:current_idx+num_files_range] = [source + '_' + str(start_idx)] * num_files_range
        current_idx += num_files_range

    return files, sources

# Create image datasets.
# Processes images and saves them in train, val, test splits.
def process_data():

    d = os.path.join(DATA_DIR, 'raw/')
    _,folders, _ = os.walk(d).next()
    print(folders)

    im_list = []
    source_list = []
    for folder in folders:
        im_dir = os.path.join(DATA_DIR, 'raw', folder)
        print(im_dir)
        _, _, files = os.walk(im_dir).next()
        sources = [folder] * len(files)
        files, sources = resample(files, sources, 3)
        im_list += [os.path.join(im_dir, f) for f in files]
        source_list += sources

    print('Creating data: ' + str(len(im_list)) + ' images')
    X = np.zeros((len(im_list),) + desired_im_sz + (3,), np.uint8)
    for i, im_file in enumerate(im_list):
        im = imread(im_file)
        X[i] = process_im(im, desired_im_sz)

    hkl.dump(X, os.path.join(DATA_DIR, 'X.hkl'))
    hkl.dump(source_list, os.path.join(DATA_DIR, 'sources.hkl'))

# resize and crop image
def process_im(im, desired_sz):
    target_ds = float(desired_sz[0])/im.shape[0]
    im = imresize(im, (desired_sz[0], int(np.round(target_ds * im.shape[1]))))
    d = int((im.shape[1] - desired_sz[1]) / 2)
    im = im[:, d:d+desired_sz[1]]
    return im

if __name__ == '__main__':
    process_data()