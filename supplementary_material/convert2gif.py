import moviepy.editor as mp
from moviepy.video.fx.all import crop
import os

if __name__ == '__main__':
    
    for root, dirs, files in os.walk(".", topdown=False):
        for name in files:
            if name[-3:] == 'mp4':
                f = os.path.join(root, name)
                f_out = f[:-4] + '.gif'
                
                clip = mp.VideoFileClip(f)
                clip = crop(clip, x1=0, y1=100, x2=640, y2=380)
                clip.write_gif(f_out)