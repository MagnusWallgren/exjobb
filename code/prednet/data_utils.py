import os
import hickle as hkl
import numpy as np
from kitti_settings import *
from keras import backend as K
from keras.preprocessing.image import Iterator

# Plotting
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.animation as animation

class BatchContainer():
    """Holds a batch of data"""
    def __init__(self, minibatch_size=None):
        self.x = None
        self.y = None
        self.current_index = 0
        self.minibatch_size = minibatch_size

    def set_batch_x(self, x):
        self.x = x
        self.current_index = 0

    def set_batch_y(self, y):
        self.y = y
        self.current_index = 0

    def get_batch(self):
        return self.x, self.y

    def get_minibatch(self):
        if self.minibatch_size is not None:
            minibatch_index_array = slice(self.current_index, self.current_index+self.minibatch_size)
        else:
            minibatch_index_array = slice(len(self.x))
        self.current_index += self.minibatch_size
        minibatch_x = [x[minibatch_index_array] for x in self.x]
        minibatch_y = [y[minibatch_index_array] for y in self.y]
        return minibatch_x, minibatch_y

# Data generator that creates sequences for input into PredNet.
class SequenceGenerator(Iterator):
    def __init__(self, data_file, source_file, nt,
                 batch_size=8, shuffle=False, seed=None,
                 output_mode='error', sequence_start_mode='all', N_seq=None,
                 data_format=K.image_data_format(), batch_container=None, training_ratio=None):
        self.X = hkl.load(data_file)  # X will be like (n_images, nb_cols, nb_rows, nb_channels)
        self.sources = hkl.load(source_file) # source for each image so when creating sequences can assure that consecutive frames are from same video
        self.nt = nt
        self.batch_size = batch_size
        self.data_format = data_format
        assert sequence_start_mode in {'all', 'unique'}, 'sequence_start_mode must be in {all, unique}'
        self.sequence_start_mode = sequence_start_mode
        assert output_mode in {'error', 'prediction', 'cwgan', 'wgan'}, 'output_mode must be in {error, prediction, cwgan, wgan}'
        self.output_mode = output_mode

        if self.data_format == 'channels_first':
            self.X = np.transpose(self.X, (0, 3, 1, 2))
        self.im_shape = self.X[0].shape

        if self.sequence_start_mode == 'all':  # allow for any possible sequence, starting from any frame that gives a long enough sequence
            self.possible_starts = np.array([i for i in range(self.X.shape[0] - self.nt) if self.sources[i] == self.sources[i + self.nt - 1]])
        elif self.sequence_start_mode == 'unique':  #create sequences where each unique frame is in at most one sequence
            curr_location = 0
            possible_starts = []
            while curr_location < self.X.shape[0] - self.nt + 1:
                if self.sources[curr_location] == self.sources[curr_location + self.nt - 1]:
                    possible_starts.append(curr_location)
                    curr_location += self.nt
                else:
                    curr_location += 1
            self.possible_starts = possible_starts

        if shuffle:
            self.possible_starts = np.random.permutation(self.possible_starts)
        if N_seq is not None and len(self.possible_starts) > N_seq:  # select a subset of sequences if want to
            self.possible_starts = self.possible_starts[:N_seq]
        self.N_sequences = len(self.possible_starts)
        if self.output_mode == 'wgan' or self.output_mode == 'cwgan':
            self.training_ratio = training_ratio
            self.minibatch_size = batch_size / self.training_ratio
            if batch_container is not None:
                self.batch_container = batch_container
        super(SequenceGenerator, self).__init__(len(self.possible_starts), batch_size, shuffle, seed)

    def _get_batches_of_transformed_samples(self, index_array):
        batch = np.zeros((len(index_array), self.nt) + self.im_shape, np.float32)
        for i, idx in enumerate(index_array):
            idx = self.possible_starts[idx]
            batch[i] = self.preprocess(self.X[idx:idx+self.nt])
        if self.output_mode == 'error':  # model outputs errors, so y should be zeros
            batch_x = batch
            batch_y = np.zeros(len(index_array), np.float32)
        elif self.output_mode == 'prediction':  # output actual pixels
            batch_x = batch
            batch_y = batch
        elif self.output_mode == 'wgan':
            # critic targets
            positive_y = np.ones((len(index_array), self.nt), dtype=np.float32)
            positive_y[:,0] = 0 # uncomment for weighting the first time step by 0
            negative_y = -positive_y
            dummy_y = np.zeros((len(index_array), self.nt), dtype=np.float32)

            # grab a minibatch for generator training step
            generator_in = batch[0::self.training_ratio]
            
            # if generator outputs errors
            generator_target = np.zeros(len(index_array), np.float32)
            generator_out = [positive_y[0::self.training_ratio], generator_target[0::self.training_ratio]]

            # grab all N_C minibatches for critic training
            critic_in = batch
            critic_out = [positive_y, negative_y, dummy_y]

            # store away critic input, target
            self.batch_container.set_batch_x(critic_in)
            self.batch_container.set_batch_y(critic_out)

            batch_x = generator_in
            batch_y = generator_out

        elif self.output_mode == 'cwgan':
            # critic targets
            positive_y = np.ones((len(index_array), self.nt), dtype=np.float32)
            positive_y[:,0] = 0 # uncomment for weighting the first time step by 0
            negative_y = -positive_y
            dummy_y = np.zeros((len(index_array), self.nt), dtype=np.float32)

            batch_tm1 = batch[:,:-1]
            
            # insert an empty frame at first time step
            tmp = np.zeros_like(batch[:,0])
            batch_tm1 = np.insert(batch_tm1, 0, tmp, axis=1)
            
            # grab a minibatch for generator training step
            generator_in = [batch[0::self.training_ratio], batch_tm1[0::self.training_ratio]]
            
            # if generator outputs errors
            generator_target = np.zeros(len(index_array), np.float32)
            generator_out = [positive_y[0::self.training_ratio], generator_target[0::self.training_ratio]]

            # grab all N_C minibatches for critic training
            critic_in = [batch, batch_tm1]
            critic_out = [positive_y, negative_y, dummy_y]
            # print(critic_out)
            # store away critic input, target
            self.batch_container.set_batch_x(critic_in)
            self.batch_container.set_batch_y(critic_out)

            batch_x = generator_in
            batch_y = generator_out

        return batch_x, batch_y

    def next(self):
        with self.lock:
            index_array = next(self.index_generator)
        return self._get_batches_of_transformed_samples(index_array)    

    def preprocess(self, X):
        return X.astype(np.float32) / 255

    def create_all(self):
        X_all = np.zeros((self.N_sequences, self.nt) + self.im_shape, np.float32)
        for i, idx in enumerate(self.possible_starts):
            X_all[i] = self.preprocess(self.X[idx:idx+self.nt])
        return X_all

if __name__ == '__main__':
    
    test_file = os.path.join(DATA_DIR, 'X_test.hkl')
    test_sources = os.path.join(DATA_DIR, 'sources_test.hkl')
    train_file = os.path.join(DATA_DIR, 'X_test.hkl')
    train_sources = os.path.join(DATA_DIR, 'sources_test.hkl')
    out_dir = os.path.join(RESULTS_SAVE_DIR, 'debug')
    
    writer = animation.FFMpegWriter()
    
    batch_size = 10
    minibatch_size = batch_size / 5
    nt = 10
    
    batch_container = BatchContainer(minibatch_size)
    generator = SequenceGenerator(train_file, train_sources, nt, batch_size=batch_size, shuffle=True, output_mode='wgan', batch_container=batch_container)

    X, Y = generator.next()
    X, Y = generator.next()
    x = X[0]
    xprev = X[1]
    ytarget = Y[1]

    nb = x.shape[0]
    nt = x.shape[1]
    nrows = x.shape[2]
    ncols = x.shape[3]
    nchannels = x.shape[4]
    nplot = 10
    
    fig1 = plt.figure()

    print("generator batches")
    for i in range(min(nb, nplot)):
        ims = []
        shp = (nrows,2*ncols,nchannels)
        for t in range(nt):
            xt = x[i,t]
            xtm1 = xprev[i,t]
            yt = ytarget[i,t]
            zt = np.zeros(shp)
            zt[:,range(ncols)] = xt
            zt[:,range(ncols,2*ncols)] = yt

            ims.append((plt.imshow(zt, cmap='gray'),))
        
        save_file = os.path.join(out_dir,'generator_batch_{}.mp4'.format(i))
        print(save_file)
        im_ani = animation.ArtistAnimation(fig1, ims, interval=50, repeat_delay=3000, blit=True)
        im_ani.save(save_file, writer=writer)
        plt.clf()

    print("generator targets")
    print("wasserstein (1st output)")
    print(Y[0])
    # print("mean average error (2nd output)")
    # print(Y[1])

    print("critic minibatches")
    for j in range(5):

        X, Y = batch_container.get_minibatch()
        x = X[0]
        xprev = X[1]
        for i in range(min(nb, nplot)):
            ims = []
            shp = (nrows,2*ncols,nchannels)
            for t in range(nt):
                xt = x[i,t]
                xtm1 = xprev[i,t]
                zt = np.zeros(shp)
                zt[:,range(ncols)] = xt
                zt[:,range(ncols,2*ncols)] = xtm1

                ims.append((plt.imshow(zt, cmap='gray'),))
            
            save_file = os.path.join(out_dir,'critic_batch_{}_{}.mp4'.format(j,i))
            print(save_file)
            im_ani = animation.ArtistAnimation(fig1, ims, interval=50, repeat_delay=3000, blit=True)
            im_ani.save(save_file, writer=writer)
            plt.clf()

    print("critic targets")
    print("wasserstein real (1st output)")
    print(Y[0])
    print("wasserstein fake (2nd output)")
    print(Y[1])
    print("wasserstein dummy (3rd output)")
    print(Y[2])