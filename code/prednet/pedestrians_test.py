# Misc
import os
import numpy as np
from six.moves import cPickle
import hickle as hkl
import datetime
from skimage.measure import compare_ssim

# Plotting
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.animation as animation

# Keras
import logging
logging.getLogger("tensorflow").setLevel(logging.ERROR)
from keras import backend as K
from keras.models import Model, model_from_json, load_model
from keras.layers import Input, Dense, Flatten

# Local
from prednet_gan import PrednetGan
from prednet import PredNet
from data_utils import SequenceGenerator, BatchContainer
from pedestrians_settings import *


def write_plots(x, y, output_dir, n_channels=3, data_format='channels_last', output_mode=None):
    '''
    Arguments:
        x,y             input output pairs of image sequences
        n_channels  number of channels to show in each plot
        data_format     channels_first or channels_last
        output_mode     output_mode used to label plot
    '''

    plot_save_dir = os.path.join(output_dir, 'prediction_plots/')
    if not os.path.exists(plot_save_dir): os.mkdir(plot_save_dir)

    stack_size = y.shape[0]
    nt = y.shape[1]
    if data_format == 'channels_first':
        n_channels_total = y.shape[2]
        nb_row = y.shape[3]
        nb_col = y.shape[4]
        y = K.reshape(y, (stack_size, nt, nb_row, nb_col, n_channels_total))
        x = K.reshape(x, (stack_size, nt, nb_row, nb_col, x.shape[2]))
    else:
        n_channels_total = y.shape[4]
        nb_row = y.shape[2]
        nb_col = y.shape[3]

    # plot in color
    if (output_mode == 'prediction' or output_mode == 'error') and x.shape[-1] > 1:
        n_channels = 1
        plot_color = True
    else:
        plot_color = False

    aspect_ratio = float(nb_row) / nb_col

    plt.figure(figsize = (nt, (1+n_channels)*aspect_ratio))
    gs = gridspec.GridSpec((1+n_channels), nt)
    gs.update(wspace=0., hspace=0.)
    # x = np.squeeze(x,-1)
    # y = np.squeeze(y,-1)

    for i in range(len(x)):
        for t in range(nt):
            plt.subplot(gs[t])
            if x.ndim > 4:
                plt.imshow(x[i,t], interpolation='none')
            else:
                plt.imshow(x[i,t], interpolation='none', cmap='gray')

            plt.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)
            if t==0: plt.ylabel('Actual', fontsize=10)

            if plot_color:
                plt.subplot(gs[t + nt])
                plt.imshow(y[i,t], interpolation='none')
                plt.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)
                if t==0: plt.ylabel(output_mode, fontsize=8)

            else:
                for ch in range(n_channels):
                    plt.subplot(gs[t + nt*(1+ch)])
                    plt.imshow(y[i,t,:,:,ch_idx[ch]], interpolation='none',cmap='gray')
                    plt.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)
                    if t==0: plt.ylabel(output_mode+'_'+str(ch_idx[ch]), fontsize=8)

        plt.savefig(plot_save_dir +  'plot_' + str(i) + '.png')
        plt.clf()


def write_animated_plots(x, y, output_dir):
    
    plot_save_dir = os.path.join(output_dir, 'prediction_plots/')
    if not os.path.exists(plot_save_dir): os.mkdir(plot_save_dir)

    writer = animation.FFMpegWriter()
    
    nrows = x.shape[2]
    ncols = x.shape[3]
    nchannels = x.shape[4]

    x = np.squeeze(x)
    y = np.squeeze(y)
    
    if nchannels==1:
        shp = (nrows,2*ncols)
    else:
        shp = (nrows,2*ncols,nchannels)
    
    fig1 = plt.figure()
    for i in range(x.shape[0]):
        ims = []
    
        for t in range(x.shape[1]):
            xt = x[i,t]
            yt = y[i,t]
            zt = np.zeros(shp)
            zt[:,range(ncols)] = xt
            zt[:,range(ncols,2*ncols)] = yt
            if np.min(zt) < 0:
                zt = (zt+1.0)/2.0
            ims.append((plt.imshow(zt,cmap='gray'),))
        
        save_file = os.path.join(plot_save_dir,'plot_{}.mp4'.format(i))
        im_ani = animation.ArtistAnimation(fig1, ims, interval=50, repeat_delay=3000, blit=True)
        im_ani.save(save_file, writer=writer)
        plt.clf()



def load_prednet_model(output_mode, nt, weights_dir=None):

    if weights_dir is None:
        weights_file = os.path.join(WEIGHTS_DIR, 'prednet_weights.hdf5')
        json_file = os.path.join(WEIGHTS_DIR, 'prednet_model.json')
    else:
        weights_file = os.path.join(weights_dir, 'prednet_weights.hdf5')
        json_file = os.path.join(weights_dir, 'prednet_model.json')

    # Load trained model
    f = open(json_file, 'r')
    json_string = f.read()
    f.close()
    
    train_model = model_from_json(json_string, custom_objects = {'PredNet': PredNet})
    train_model.load_weights(weights_file)
    layer_config = train_model.layers[1].get_config()
    
    layer_config['output_mode'] = output_mode

    data_format = layer_config['data_format']
    test_prednet = PredNet(weights=train_model.layers[1].get_weights(), **layer_config)
    input_shape = list(train_model.layers[0].batch_input_shape[1:])
    input_shape[0] = nt
    
    return test_prednet, input_shape, data_format

def compute_metrics(x, y):
    assert x.shape == y.shape, 'x.shape must be equal to y.shape.'
    metrics = {}

    # look at all timesteps except the first
    metrics['mse'] = np.mean( (x[:, 1:] - y[:, 1:])**2 )  
    metrics['mae'] = np.mean( np.abs((x[:, 1:] - y[:, 1:])) )
    ssim = []
    for xi, yi in zip(x, y):
        for xt, yt in zip(xi[1:], yi[1:]):
            ssim.append(compare_ssim(xt, yt, multichannel=True))
    metrics['ssim'] = np.mean(ssim)
    
    return metrics

def test_generator(weights_dir, output_dir=None, plot_idx=[49, 148, 788]):

    # n_plot = 100
    nt = 10
    output_mode = 'prediction'

    test_file = os.path.join(DATA_DIR, 'X_test.hkl')
    test_sources = os.path.join(DATA_DIR, 'sources_test.hkl')

    # load model
    test_prednet, input_shape, data_format = load_prednet_model(output_mode=output_mode, nt=nt, weights_dir=weights_dir)
    
    output_shape = test_prednet.compute_output_shape(input_shape)
    inputs = Input(shape=tuple(input_shape))
    predictions = test_prednet(inputs)
    test_model = Model(inputs=inputs, outputs=predictions)
    
    # Create testing model (to output predictions)
    test_generator = SequenceGenerator(test_file, test_sources, nt, sequence_start_mode='unique', data_format=data_format, output_mode='prediction')
    X_test = test_generator.create_all()
    Y_test = test_model.predict(X_test)
    
    print("Computing metrics")
    metrics = compute_metrics(X_test, Y_test)

    # Plot some predictions
    print('Writing plots')
    if output_dir is not None and DATASET_NAME == 'caltech_pedestrians':
        write_animated_plots(X_test[plot_idx], Y_test[plot_idx], output_dir)
        write_plots(X_test[plot_idx], Y_test[plot_idx], output_dir, n_channels=3, data_format='channels_last', output_mode='prediction')

    return metrics

def test_dummy_generator(output_dir):
    # n_plot = 100
    nt = 10
    output_mode = 'prediction'

    test_file = os.path.join(DATA_DIR, 'X_test.hkl')
    test_sources = os.path.join(DATA_DIR, 'sources_test.hkl')
    
    # Create testing model (to output predictions)
    test_generator = SequenceGenerator(test_file, test_sources, nt, sequence_start_mode='unique', output_mode='prediction')
    X_test = test_generator.create_all()
    Y_test = np.zeros_like(X_test)
    Y_test[:,1:] = X_test[:,:-1]
    
    print("Computing metrics")
    metrics = compute_metrics(X_test, Y_test)
    return metrics

def roc_curve(y_real, y_fake, output_dir):
    
    y_real = y_real.flatten()
    y_fake = y_fake.flatten()

    lower = np.min([np.min(y_real), np.min(y_fake)])
    upper = np.max([np.max(y_real), np.max(y_fake)])

    
    # tics = np.linspace(lower - 1e-2, upper + 1e2, m)
    tics = np.sort(np.concatenate((y_real, y_fake, upper + 1.0), axis=None) - 1e-3)
    tpr = np.zeros(len(tics))
    fpr = np.zeros(len(tics))
    n = float(len(y_real))
    for i, threshold in enumerate(tics):
        tp = np.sum(y_real < threshold)
        fp = np.sum(y_fake < threshold)
        tpr[i] = tp / n
        fpr[i] = fp / n
    curve_area = np.trapz(tpr, fpr)

    plt.plot(fpr, tpr,'-',markersize=10, label='roc')
    plt.plot(np.linspace(0,1,2), np.linspace(0,1,2),'-',markersize=10, label='y=x')
    plt.xlabel('fpr', fontsize=10)
    plt.ylabel('tpr', fontsize=10)
    # plt.axis('equal')
    plt.axis([0, 1, 0, 1])
    plt.title("ROC")
    plt.legend(loc='best')
    # plt.tight_layout()
    plt.savefig(os.path.join(output_dir, 'roc_curve.png'))
    plt.clf()
    return curve_area

def compute_critic_metrics(real, fake):
    metrics = {}
    metrics['yt_real'] = [np.mean(real[:,0]), np.mean(real[:,1:])]
    metrics['yt_fake'] = [np.mean(fake[:,0]), np.mean(fake[:,1:])]
    return metrics

def test_critic(weights_dir, output_dir, use_conditioning=True):

    # Data parameters
    n_channels = 3
    im_height = 128
    im_width = 160
    input_shape = (n_channels, im_height, im_width) if K.image_data_format() == 'channels_first' else (im_height, im_width, n_channels)
    nt = 10
    
    # Model parameters
    layer_loss_weights = np.array([1., 0., 0., 0.])  # weighting for each layer in final loss; "L_0" model:  [1, 0, 0, 0], "L_all": [1, 0.1, 0.1, 0.1]
    layer_loss_weights = np.expand_dims(layer_loss_weights, 1)
    time_loss_weights = 1./ (nt - 1) * np.ones((nt,1))  # equally weight all timesteps except the first
    time_loss_weights[0] = 0
    stack_sizes = (n_channels, 48, 96, 192)
    A_filt_sizes = (3, 3, 3)
    Ahat_filt_sizes = (3, 3, 3 ,3)
    R_filt_sizes = (3, 3, 3, 3)

    # data files
    test_file = os.path.join(DATA_DIR, 'X_test.hkl')
    test_sources = os.path.join(DATA_DIR, 'sources_test.hkl')

    # weights files
    weights_file_generator = os.path.join(weights_dir, 'generator_model.hdf5')
    weights_file_critic = os.path.join(weights_dir, 'critic_model.hdf5')

    gan = PrednetGan(input_shape, stack_sizes, layer_loss_weights, time_loss_weights, 
        A_filt_sizes=A_filt_sizes, Ahat_filt_sizes=Ahat_filt_sizes, R_filt_sizes=R_filt_sizes, nt=nt, use_conditioning=use_conditioning)
    
    gan.build_gan()
    gan.load_gan(weights_file_critic=weights_file_critic, weights_file_generator=weights_file_generator)

    val_generator = SequenceGenerator(test_file, test_sources, nt, sequence_start_mode='unique', output_mode='prediction')
    x = val_generator.create_all()
    if DATASET_NAME == 'caltech_pedestrians':
        x = x[0::15] # 12 would be optimal
    if use_conditioning:
        x_tm1 = np.zeros_like(x)
        for i in range(x.shape[0]):
            x_tm1[i,1:] = x[i,:-1]
        inputs = [x, x_tm1]
    else:
        inputs = x

    # positive_y = np.ones((x.shape[0], nt), dtype=np.float32)
    # dummy_y = positive_y * 0
    # outputs = [positive_y, positive_y, dummy_y]
    metrics = {}
    
    predictions = gan.critic_model.predict(inputs)
    metrics = compute_critic_metrics(predictions[0], predictions[1])
    area = roc_curve(predictions[0], predictions[1], output_dir)
    metrics['area'] = area
    return metrics


def write_log(output_dir, metrics):
    print("Writing log")
    f = open(os.path.join(output_dir, 'output.log'), 'w')
    for key in metrics.keys():
        f.write('{:<10}{:<10}\n'.format(key, metrics[key]))
    f.close()

if __name__ == '__main__':
    
    print(DATASET_NAME)
    ############################ TESTING #############################
    # dir_names_list = ['190225_0']
    dir_names_list = ['190214_0', '190215_0', '190219_2', '190220_0', '190221_0', '190225_0']
    plot_idx = [49, 148, 788]
    for dir_name in dir_names_list:
        if not os.path.exists(os.path.join(RESULTS_SAVE_DIR, dir_name)):
            print("DIRECTORY NOT FOUND " + dir_name)
        else:
            weights_dir = os.path.join(RESULTS_SAVE_DIR, dir_name, 'prednet_kitti')
            output_dir = os.path.join(RESULTS_SAVE_DIR, dir_name, 'test_results')
            print(output_dir)
            print("Testing model " + weights_dir)
            critic_metrics = None
            generator_metrics = None
            # 1. Animated sequences
            # 2. Panorama sequences
            if not os.path.exists(output_dir): os.mkdir(output_dir)
            # print("Testing generator")
            # generator_metrics = test_generator(weights_dir, output_dir=output_dir, plot_idx=plot_idx)

            # 3. Evaluate generator on metrics
            # 4. ROC curve if critic present
            if os.path.exists(os.path.join(RESULTS_SAVE_DIR, dir_name, 'prednet_kitti', 'critic_model.hdf5')):
                print("Testing critic")
                if dir_name == '190221_0':
                    critic_metrics = test_critic(weights_dir, output_dir, use_conditioning=False)
                    # metrics['roc_area'] = test_critic(weights_dir, output_dir, use_conditioning=False)
                else:
                    critic_metrics = test_critic(weights_dir, output_dir, use_conditioning=True)
                    # metrics['roc_area'] = test_critic(weights_dir, output_dir, use_conditioning=True)
            metrics = {}
            if generator_metrics is not None:
                for key in generator_metrics: metrics[key] = generator_metrics[key]
            if critic_metrics is not None:
                for key in critic_metrics: metrics[key] = critic_metrics[key]
            write_log(output_dir, metrics)

    # Dummy tests
    print("Testing dummy model")
    output_dir = os.path.join(RESULTS_SAVE_DIR, 'baseline', 'test_results')
    print(output_dir)
    if not os.path.exists(output_dir): os.mkdir(output_dir)
    metrics = test_dummy_generator(output_dir)
    write_log(output_dir, metrics)
