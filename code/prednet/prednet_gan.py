'''
Train PredNet
'''

import warnings
import os
import numpy as np
np.random.seed(123)
from six.moves import cPickle

import logging
logging.getLogger("tensorflow").setLevel(logging.ERROR)
from keras import backend as K
from keras.models import Model, model_from_json, Sequential, load_model
from keras.layers import Input, Dense, Flatten, Concatenate, Layer
from keras.layers import Convolution2D
from keras.layers import TimeDistributed
from keras.layers.merge import _Merge
from keras.layers.advanced_activations import LeakyReLU
from keras.callbacks import ModelCheckpoint, Callback, TensorBoard
from keras.optimizers import Adam
from keras import losses
from functools import partial
from prednet import PredNet
from data_utils import SequenceGenerator, BatchContainer
from kitti_settings import *

warnings.filterwarnings(action="ignore", category=UserWarning, message="Method on_batch_begin", module="keras")
warnings.filterwarnings(action="ignore", category=UserWarning, message="Discrepancy between trainable weights and collected trainable weights", module="keras")


class UpdateCritic(Callback):
    """Runs critic updates at beginning of epoch"""
    def __init__(self, critic_model=None, training_ratio=5, batch_container=None, val_generator=None, initial_epoch=0, log_batch=False):
        self.critic_model = critic_model
        self.training_ratio = training_ratio
        self.batch_container = batch_container
        self.val_generator = val_generator
        self.initial_epoch = initial_epoch
        self.log_batch = log_batch
        self.gen_loss = {'epoch': [],
                         'total': [],
                         'wass': [],
                         'mae': []
                        }
        self.critic_loss = {'epoch': [],
                            'total': [],
                            'real': [],
                            'fake': [],
                            'gp': [],
                            'acc_real': [],
                            'acc_fake': []
                            }
        if self.log_batch:
            self.batch_loss = {'total': [],
                               'real': [],
                               'fake': [],
                               'gp': []
                                }
    def on_batch_begin(self, batch, logs=None):
        for i in range(self.training_ratio):
            minibatch_x, minibatch_y = self.batch_container.get_batch()
            loss = self.critic_model.train_on_batch(minibatch_x, minibatch_y)
            if self.log_batch:
                self.batch_loss['total'].append(loss[0])
                self.batch_loss['real'].append(loss[1])
                self.batch_loss['fake'].append(loss[2])
                self.batch_loss['gp'].append(loss[3])

#['loss', 'y_real_loss', 'y_fake_loss', 'y_avg_loss', 'y_real_classification_accuracy', 'y_fake_classification_accuracy', 'y_avg_classification_accuracy']
    def on_epoch_end(self, epoch, logs=None):
        total_epoch = self.initial_epoch + epoch
        # evaluate generator model on val_generator, and update val_batch_container
        generator_loss = self.model.evaluate_generator(self.val_generator)
        self.gen_loss['epoch'].append(total_epoch)
        self.gen_loss['total'].append(generator_loss[0])
        self.gen_loss['wass'].append(generator_loss[1])
        self.gen_loss['mae'].append(generator_loss[2])
        
        # evaluate critic model on data in batch_container
        batch_x, batch_y = self.batch_container.get_batch()
        critic_loss = self.critic_model.test_on_batch(batch_x, batch_y)
        self.critic_loss['epoch'].append(total_epoch)
        self.critic_loss['total'].append(critic_loss[0])
        self.critic_loss['real'].append(critic_loss[1])
        self.critic_loss['fake'].append(critic_loss[2])
        self.critic_loss['gp'].append(critic_loss[3])
        self.critic_loss['acc_real'].append(critic_loss[4])
        self.critic_loss['acc_fake'].append(critic_loss[5])

        
def wasserstein_loss(y_true, y_pred):
    return K.mean(y_true * y_pred)

def classification_accuracy(y_true, y_pred):
    x = y_pred * y_true
    x = K.less(x, 0)
    x = K.cast(x, "float32")
    x = K.mean(x)
    # x = K.mean(x) * 10.0 / 9.0 # don't count the first point
    return x

def wasserstein_values(y_true, y_pred):
    return y_true * y_pred

def gradient_penalty_loss(y_true, y_pred, averaged_samples, gradient_penalty_weight):
    gradients = K.gradients(y_pred, averaged_samples)[0]
    gradients_sqr = K.square(gradients)
    gradients_sqr_sum = K.sum(gradients_sqr, axis=np.arange(1, len(gradients_sqr.shape)))
    gradient_l2_norm = K.sqrt(gradients_sqr_sum)
    gradient_penalty = gradient_penalty_weight * K.square(1 - gradient_l2_norm)
    return K.mean(gradient_penalty)

class RandomWeightedAverage(_Merge):

    def _merge_function(self, inputs):
        input_shape = K.shape(inputs[0])
        weights = K.random_uniform((input_shape[0], input_shape[1], 1, 1, 1))
        return (weights * inputs[0]) + ((1 - weights) * inputs[1])


class OutputSlicer(Layer):
    def __init__(self, shp, nlayers, **kwargs):
        self.shp = shp
        self.nlayers = nlayers
        self.numel_x = self.shp[1]*self.shp[2]*self.shp[3]
        super(OutputSlicer, self).__init__(**kwargs)

    def build(self, input_shape):
        super(OutputSlicer, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        xhat = K.reshape(x[:,:,:self.numel_x], (-1,) + self.shp) # nb_batches, nt, image_shape
        e = K.reshape(x[:,:,self.numel_x:], (-1, self.shp[0], self.nlayers)) # nb_batches, nt, nlayers
        return [xhat, e]
        # e = K.reshape(x[:,:,self.numel_x:], (-1, self.shp[0], self.nlayers)) # nb_batches, nt, nlayers
        # return e

    def compute_output_shape(self, input_shape):
        x_shape = (input_shape[0],) + self.shp
        e_shape = (input_shape[0], input_shape[1], self.nlayers)
        return [x_shape, e_shape]
        # e_shape = (input_shape[0], input_shape[1], self.nlayers)
        # return e_shape





class PrednetGan():
    def __init__(self, input_shape, stack_sizes, 
                 layer_loss_weights, time_loss_weights,
                 A_filt_sizes=(3,3,3), Ahat_filt_sizes=(3,3,3,3), R_filt_sizes=(3,3,3,3), 
                 data_format=K.image_data_format(), nt=10, training_ratio=5, gradient_penalty_weight=10,
                 tb_dir=None, critic_metrics=None, use_conditioning=True):
        self.critic = None
        self.critic_model = None
        self.critic_metrics = critic_metrics
        self.generator_model = None
        self.input_shape = input_shape
        self.data_format = data_format
        self.channel_axis = -3 if self.data_format == 'channels_first' else -1
        self.stack_sizes = stack_sizes
        self.R_stack_sizes = stack_sizes
        self.A_filt_sizes = A_filt_sizes
        self.Ahat_filt_sizes = Ahat_filt_sizes
        self.R_filt_sizes = R_filt_sizes
        self.nt = nt
        self.training_ratio = training_ratio
        self.gradient_penalty_weight = gradient_penalty_weight
        self.layer_loss_weights = layer_loss_weights
        if time_loss_weights is None:
            self.time_loss_weights = 1./ (nt - 1) * np.ones((nt))  # equally weight all timesteps except the first
            self.time_loss_weights[0] = 0
        else:
            self.time_loss_weights = time_loss_weights
        self.generator_loss_weight = [1, 100] # weighting between critic loss and l1-error
        self.use_critic = self.generator_loss_weight[0] # don't train critic model if its loss weight is 0
        self.lr_critic = 1e-4
        self.lr_generator = 1e-4
        self.tb_dir = tb_dir
        self.use_conditioning = use_conditioning

    def make_critic(self, input_shape):
        input_shape = self.input_shape
        with K.name_scope('critic'):
            model = Sequential()
            if self.use_conditioning:
                if self.data_format == 'channels_first':
                    model.add(Convolution2D(16, (3, 3), padding='same', input_shape=(2*input_shape[0], input_shape[1], input_shape[2])))
                else:
                    model.add(Convolution2D(16, (3, 3), padding='same', input_shape=(input_shape[0], input_shape[1], 2*input_shape[2])))
            else:
                model.add(Convolution2D(16, (3, 3), padding='same', input_shape=(input_shape[0], input_shape[1], input_shape[2])))
            model.add(LeakyReLU())
            model.add(Convolution2D(32, (3, 3), kernel_initializer='he_normal', strides=[2, 2]))
            model.add(LeakyReLU())
            model.add(Convolution2D(32, (3, 3), kernel_initializer='he_normal', padding='same', strides=[2, 2]))
            model.add(LeakyReLU())
            model.add(Flatten())
            model.add(Dense(128, kernel_initializer='he_normal'))
            model.add(LeakyReLU())
            model.add(Dense(1, kernel_initializer='he_normal'))
        self.critic = model

        return

    def make_patch_critic(self, input_shape):

        def d_layer(layer_input, filters, f_size=4, bn=False):
            """Discriminator layer"""
            d = Conv2D(filters, kernel_size=f_size, strides=2, padding='same')(layer_input)
            d = LeakyReLU(alpha=0.2)(d)
            if bn:
                d = BatchNormalization(momentum=0.8)(d)
            return d

        x = Input(shape=(self.nt,) + self.input_shape)
        x_tm1 = Input(shape=(self.nt,) + self.input_shape)

        # Concatenate image and conditioning image by channels to produce input
        combined_imgs = Concatenate(axis=-1)([x, x_tm1])
        nf = 2
        d1 = d_layer(combined_imgs, nf, f_size=4, strides=2) # 70
        d2 = d_layer(d1, nf, f_size=4, strides=2) # 34
        d3 = d_layer(d2, nf*2, f_size=4, strides=2) # 16
        d4 = d_layer(d3, nf*4, f_size=4, strides=2) # 7
        d5 = d_layer(d4, nf*8, f_size=4, strides=1) # 4
        y = Conv2D(1, kernel_size=4, strides=1, padding='same')(d5) # 1

        self.critic = Model(inputs=[x, x_tm1], outputs=y)



    def make_generator(self):
        prednet = PredNet(self.stack_sizes, self.R_stack_sizes, self.A_filt_sizes, self.Ahat_filt_sizes, self.R_filt_sizes,
                  output_mode='all', return_sequences=True, name='prednet')
        return prednet


    def build_gan(self):
        if self.use_critic:

            generator = self.make_generator()
            self.make_critic(self.input_shape)

            for layer in self.critic.layers:
                layer.trainable = False
            self.critic.trainable = False
            self.build_generator(generator)

            for layer in self.critic.layers:
                layer.trainable = True
            for layer in self.generator_model.layers:
                layer.trainable = False
            self.critic.trainable = True
            self.generator_model.trainable = False
            
            x_in = Input((self.nt,) + self.input_shape)
            generator_out = generator(x_in)
            out_list = OutputSlicer((self.nt,) + self.input_shape, len(self.stack_sizes))(generator_out)
            x_fake = out_list[0]
            x_real = x_in
            x_avg = RandomWeightedAverage()([x_fake, x_real])

            if self.use_conditioning:
                x_tm1 = Input((self.nt,) + self.input_shape)

            with K.name_scope("fake"):
                if self.use_conditioning:
                    x_fake = Concatenate(axis=self.channel_axis)([x_fake, x_tm1])
                y_fake = TimeDistributed(self.critic)(x_fake)
                y_fake = Flatten(name='y_fake')(y_fake)
            
            with K.name_scope("real"):
                if self.use_conditioning:
                    x_real = Concatenate(axis=self.channel_axis)([x_real, x_tm1])
                y_real = TimeDistributed(self.critic)(x_real)
                y_real = Flatten(name='y_real')(y_real)

            with K.name_scope("avg"):
                if self.use_conditioning:
                    x_avg = Concatenate(axis=self.channel_axis)([x_avg, x_tm1])
                y_avg = TimeDistributed(self.critic)(x_avg)
                y_avg = Flatten(name='y_avg')(y_avg)

            # gradient penalty loss
            partial_gp_loss = partial(gradient_penalty_loss,
                                      averaged_samples=x_avg, 
                                      gradient_penalty_weight=self.gradient_penalty_weight)
            partial_gp_loss.__name__ = 'gradient_penalty'
            if self.use_conditioning:
                self.critic_model = Model(inputs=[x_in, x_tm1], outputs=[y_real, y_fake, y_avg])
            else:
                self.critic_model = Model(inputs=x_in, outputs=[y_real, y_fake, y_avg])
            
            if self.critic_metrics == 'wasserstein_values':
                metrics = [wasserstein_values]
            else:
                metrics = None
            self.critic_model.compile(optimizer=Adam(self.lr_critic, beta_1=0.0, beta_2=0.9),
                loss=[wasserstein_loss, wasserstein_loss, partial_gp_loss],
                metrics=metrics)
            
        else:
            generator = self.make_generator()
            self.build_generator(generator)

        return

    def build_generator(self, generator):
        
        x = Input(shape=(self.nt,) + self.input_shape)
        out_all = generator(x)
        out_list = OutputSlicer((self.nt,) + self.input_shape, len(self.stack_sizes))(out_all) # errors will be (batch_size, nt, nb_layers)
        predictions = out_list[0]
        errors = out_list[1]
        errors_by_time = TimeDistributed(Dense(1, trainable=False), weights=[self.layer_loss_weights, np.zeros(1)], trainable=False)(errors)  # calculate weighted error by layer
        errors_by_time = Flatten()(errors_by_time)  # will be (batch_size, nt)
        final_errors = Dense(1, weights=[self.time_loss_weights, np.zeros(1)], trainable=False, name='mae')(errors_by_time)  # weight errors by time
        
        if self.use_critic:
            if self.use_conditioning:
                x_tm1 = Input(shape=(self.nt,) + self.input_shape)
                predictions = Concatenate(axis=self.channel_axis)([predictions, x_tm1])
            critic_value = TimeDistributed(self.critic)(predictions)
            critic_value = Flatten(name='critic')(critic_value)
            if self.use_conditioning:
                self.generator_model = Model(inputs=[x, x_tm1], outputs=[critic_value, final_errors])
            else:
                self.generator_model = Model(inputs=x, outputs=[critic_value, final_errors])
            self.generator_model.compile(loss=[wasserstein_loss, 'mean_absolute_error'], 
                optimizer=Adam(self.lr_critic, beta_1=0.0, beta_2=0.9), 
                loss_weights=self.generator_loss_weight)
        else:
            self.generator_model = Model(inputs=x, outputs=final_errors)
            self.generator_model.compile(loss='mean_absolute_error', optimizer='adam')
        return


    def train_gan(self, nb_epoch, batch_size, samples_per_epoch, N_seq_val, resume_training=True):
        """"""
        # Data files
        train_file = os.path.join(DATA_DIR, 'X_train.hkl')
        train_sources = os.path.join(DATA_DIR, 'sources_train.hkl')
        val_file = os.path.join(DATA_DIR, 'X_val.hkl')
        val_sources = os.path.join(DATA_DIR, 'sources_val.hkl')

        # weights files
        weights_file_generator = os.path.join(WEIGHTS_DIR, 'generator_model.hdf5')
        json_file_generator = os.path.join(WEIGHTS_DIR, 'generator_model.json')
        weights_file_test = os.path.join(WEIGHTS_DIR, 'prednet_weights.hdf5')
        json_file_test = os.path.join(WEIGHTS_DIR, 'prednet_model.json')
        training_info_file = os.path.join(WEIGHTS_DIR, 'training_info.txt')
        if self.use_critic:
            weights_file_critic = os.path.join(WEIGHTS_DIR, 'critic_model.hdf5')
            json_file_critic = os.path.join(WEIGHTS_DIR, 'critic_model.json')
        else:
            weights_file_critic = None
            json_file_critic = None

        self.build_gan()
        if resume_training:
            current_epoch = self.load_gan(weights_file_critic=weights_file_critic, weights_file_generator=weights_file_generator, training_info_file=training_info_file)
        else:
            current_epoch = 0
        
        if self.use_critic:
            self.critic_model.summary()
        self.generator_model.summary()

        if self.use_critic:
            minibatch_size = batch_size
            batch_size = batch_size * self.training_ratio
            batch_container = BatchContainer(minibatch_size)
            if self.use_conditioning:
                output_mode = 'cwgan'
            else:
                output_mode = 'wgan'    
        else:
            batch_container = None
            output_mode = 'error'
        train_generator = SequenceGenerator(train_file, train_sources, self.nt, batch_size=batch_size, shuffle=True, output_mode=output_mode, batch_container=batch_container, training_ratio=self.training_ratio)
        val_generator = SequenceGenerator(val_file, val_sources, self.nt, batch_size=batch_size, output_mode=output_mode, N_seq=N_seq_val, batch_container=batch_container, training_ratio=self.training_ratio)
        
        callbacks = []
        save_model = True
        if save_model:
            if not os.path.exists(WEIGHTS_DIR): os.mkdir(WEIGHTS_DIR)
            callbacks.append(ModelCheckpoint(filepath=weights_file_generator))
        if self.tb_dir is not None: 
            callbacks.append(TensorBoard(log_dir=self.tb_dir, write_graph=True, write_grads=False, write_images=False))
        if self.use_critic:
            critic_cb = UpdateCritic(critic_model=self.critic_model, training_ratio=self.training_ratio, batch_container=batch_container, val_generator=val_generator, initial_epoch=current_epoch, log_batch=True)
            callbacks.append(critic_cb)

        
        if self.use_critic:
            # don't add val_generator to model if using critic, validation done in callback instead
            history = self.generator_model.fit_generator(train_generator, samples_per_epoch / batch_size, nb_epoch, callbacks=callbacks)
        else:
            history = self.generator_model.fit_generator(train_generator, samples_per_epoch / batch_size, nb_epoch, validation_data=val_generator, callbacks=callbacks)
        current_epoch += nb_epoch
        if self.use_critic:
            history.history['critic_loss'] = critic_cb.critic_loss
            history.history['gen_loss'] = critic_cb.gen_loss
            history.history['batch_loss'] = critic_cb.batch_loss

        if save_model:
            self.save_gan(weights_file_critic=weights_file_critic, json_file_critic=json_file_critic, 
                weights_file_generator=weights_file_generator, json_file_generator=json_file_generator, 
                weights_file_test=weights_file_test, json_file_test=json_file_test,
                training_info_file=training_info_file, current_epoch=current_epoch)

        return history


        
    def save_gan(self, weights_file_critic=None, json_file_critic=None, weights_file_generator=None, json_file_generator=None, weights_file_test=None, json_file_test=None, training_info_file=None, current_epoch=None):
        """Saves gan generator and critic and a testing model"""
        if training_info_file is not None:
            with open (training_info_file, 'w') as f:
                if current_epoch is not None:
                    f.write(str(current_epoch))

        if weights_file_critic is not None:
            self.critic_model.save_weights(weights_file_critic)
        if json_file_critic is not None:
            json_string = self.critic_model.to_json()
            with open(json_file_critic, "w") as f:
                f.write(json_string)
        
        if weights_file_generator is not None:
            self.generator_model.save_weights(weights_file_generator)
        if json_file_generator is not None:
            json_string = self.generator_model.to_json()
            with open(json_file_generator, "w") as f:
                f.write(json_string)

        if weights_file_test is not None or json_file_test is not None:
            # testing model
            prednet = PredNet(self.stack_sizes, self.R_stack_sizes,
                        self.A_filt_sizes, self.Ahat_filt_sizes, self.R_filt_sizes,
                        output_mode='prediction', A_activation='relu', return_sequences=True)

            inputs = Input(shape=(self.nt,) + self.input_shape)
            predictions = prednet(inputs)  # errors will be (batch_size, nt, nb_layers)
            model = Model(inputs=inputs, outputs=predictions)
            model.compile(loss='mae', optimizer='adam')
            model.layers[1].set_weights(self.generator_model.layers[1].get_weights())
        if weights_file_test is not None:
            model.save_weights(weights_file_test)
        if json_file_test is not None:
            json_string = model.to_json()
            with open(json_file_test, "w") as f:
                f.write(json_string)

    def load_gan(self, weights_file_critic=None, json_file_critic=None, weights_file_generator=None, json_file_generator=None, training_info_file=None):
        """"""
        if training_info_file is not None:
            f = open(training_info_file, 'r')
            s = f.read()
            f.close()
            current_epoch = int(s)
        else:
            current_epoch = 0

        if json_file_generator is not None:
            f = open(json_file_generator, 'r')
            json_string = f.read()
            f.close()
            self.generator_model = model_from_json(json_string, custom_objects = {'PredNet': PredNet, 'RandomWeightedAverage': RandomWeightedAverage})
        if weights_file_generator is not None: 
            self.generator_model.load_weights(weights_file_generator)

        if json_file_critic is not None:
            f = open(json_file_critic, 'r')
            json_string = f.read()
            f.close()
            self.critic_model = model_from_json(json_string, custom_objects = {'PredNet': PredNet, 'RandomWeightedAverage': RandomWeightedAverage})
        if weights_file_critic is not None: 
            self.critic_model.load_weights(weights_file_critic)

        return current_epoch