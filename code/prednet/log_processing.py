import os
import numpy as np
import string

# Plotting
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.animation as animation

class Log(object):
    """Log"""
    def __init__(self):
        self.data = {'critic': {}, 'generator': {}}
        self.header = ''
    def process_log(self, log_file):
        print('processing ' + log_file)
        f = open(log_file, 'r')
        s = f.read()
        f.close()

        hbp = string.find(s, "# training history")  # header breakpoint
        dbp = string.find(s, "generator_loss")      # data breakpoint
        if dbp < 0:
            self.data['generator'] = self.raw2data(s[hbp:dbp], column_format='map')
        else:
            if len(self.header) > 0:
                critic_data = self.raw2data(s[hbp:dbp], column_format='header')
                for key in critic_data.keys(): self.data['critic'][key] += critic_data[key]
                generator_data = self.raw2data(s[dbp:], column_format='header')
                for key in generator_data.keys(): self.data['generator'][key] += generator_data[key]
            else:
                self.data['critic'] = self.raw2data(s[hbp:dbp], column_format='header')
                self.data['generator'] = self.raw2data(s[dbp:], column_format='header')
        self.header = s[:hbp]

    def raw2data(self, s, column_format='header'):
        if column_format == 'header':
            lines = string.split(s, "\n")
            fields = string.split(lines[1])
            data = {key: [] for key in fields}
            for l in lines[2:]:
                values = string.split(l)
                for i, val in enumerate(values):
                    data[fields[i]].append(float(val))
        elif column_format == 'map':
            lines = string.split(s, "\n")
            field = 'mae'
            data = {field: []}
            for l in lines[1:]:
                values = string.split(l)
                if len(values) < 2:
                    break
                data[field].append(float(values[2]))
            data['epoch'] = np.arange(len(data['mae'])) + 1
        return data

    def plot_loss(self, output_dir):

        #########################   CRITIC   #########################
        if len(self.data['critic']) > 0:
            epoch = self.data['critic']['epoch']
            total = np.asarray(self.data['critic']['total'])
            real = np.asarray(self.data['critic']['real'])
            fake = np.asarray(self.data['critic']['fake'])
            gp = np.asarray(self.data['critic']['gp'])

            # critic basic plot: total and gp
            f = os.path.join(output_dir, 'critic_basic.png')
            plt.plot(epoch, gp,'-', label='gp')
            plt.plot(epoch, real+fake, '-', label='real-fake')
            plt.plot(epoch, total, '-', label='total')
            bottom, top = plt.ylim()
            dy = max(np.abs(bottom), np.abs(top))
            if bottom < 0:
                plt.ylim((-dy, dy))   
            else:
                plt.ylim((0, dy))
            plt.xlabel('epoch')
            plt.ylabel('loss')
            plt.title('Critic Training Loss')
            plt.legend()
            plt.savefig(f)
            plt.clf()

            # critic extra plot: total, gp, real, fake
            f = os.path.join(output_dir, 'critic_extra.png')
            # plt.plot(epoch, total, '-', label='total')
            plt.plot(epoch, real, '-', label='real')
            plt.plot(epoch, -fake, '-', label='-fake')
            # plt.plot(epoch, gp,'-', label='gp')
            bottom, top = plt.ylim()
            dy = max(np.abs(bottom), np.abs(top))
            if bottom < 0:
                plt.ylim((-dy, dy))   
            else:
                plt.ylim((0, dy))
            plt.xlabel('epoch')
            plt.ylabel('loss')
            plt.title('Critic Training Loss')
            plt.legend()
            plt.savefig(f)
            plt.clf()

        ######################### GENERATOR #########################
        if len(self.data['generator']) > 0:
            epoch = self.data['generator']['epoch']
            mae = np.asarray(self.data['generator']['mae'])
            if 'total' in self.data['generator'].keys():
                total = np.asarray(self.data['generator']['total'])
            else:
                total = None
            if 'wass' in self.data['generator'].keys():
                wass = np.asarray(self.data['generator']['wass'])
            else:
                wass = None

            # generator basic plot: mae
            f = os.path.join(output_dir, 'generator_basic.png')
            plt.plot(epoch, mae, '-', label='mae')
            plt.xlabel('epoch')
            plt.ylabel('loss')
            bottom, top = plt.ylim()
            dy = max(np.abs(bottom), np.abs(top))
            if bottom < 0:
                plt.ylim((-dy, dy))   
            else:
                plt.ylim((0, dy))
            plt.title('Generator Training Loss')
            plt.legend()
            plt.savefig(f)
            plt.clf()

            # generator extra plot: mae, wass, total
            if wass is not None and total is not None:
                f = os.path.join(output_dir, 'generator_extra.png')
                plt.plot(epoch, total, '-', label='total')
                plt.plot(epoch, wass, '-', label='wass')
                plt.plot(epoch, 100*mae, '-', label='mae')
                bottom, top = plt.ylim()
                dy = max(np.abs(bottom), np.abs(top))
                if bottom < 0:
                    plt.ylim((-dy, dy))   
                else:
                    plt.ylim((0, dy))
                plt.xlabel('epoch')
                plt.ylabel('loss')
                plt.title('Generator Training Loss')
                plt.legend()
                plt.savefig(f)
                plt.clf()

if __name__ == '__main__':

    d = '/Users/magnuswallgren/Documents/Skolarbete/exjobb/report/figures/'
    _, folders, _ = os.walk(d).next()
    for f in folders:
        log_file = os.path.join(d, f, 'output.log')
        log_file2 = os.path.join(d, f, 'output2.log')
        if os.path.exists(log_file):
            output_dir = os.path.join(d, f, 'plots')
            if not os.path.exists(output_dir): os.mkdir(output_dir)
            log = Log()
            if os.path.exists(log_file2):
                log.process_log(log_file2)
                log.process_log(log_file)
            else:
                log.process_log(log_file)
            log.plot_loss(output_dir)
